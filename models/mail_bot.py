# -*- coding: utf-8 -*-
# Part of Aroolla. See LICENSE file for full copyright and licensing details.
from datetime import datetime
import random
import re
import json
from json.decoder import JSONDecodeError
import logging

from odoo.http import request
from odoo import models, _, fields

_logger = logging.getLogger(__name__)

class MailBot(models.AbstractModel):
    _inherit = 'mail.bot'

    def send_message_to_tour(self,message):
        channel = (self._cr.dbname, 'aroolla_base_bot.mail.bot', request.session.uid)
        _logger.debug('bus send [{}] => {}'.format(channel, message))
        self.env['bus.bus'].sendone(channel, message)

    def format_msg(self, msg, values=None):
        # init msg_dict with global vals
        # TODO: set actual values
        msg_dict = {'name': self.env.user.name,
                    'chrono': 10,
                    'total_participants': 20,
                    'ranking': 1
                    }
        if values and 'bot_msg_dict' in values:
            # get msg specific variables from verif function in values
            msg_dict.update(values['bot_msg_dict'])
        try:
            formatted_msg = msg.format(**msg_dict) if msg else None
        except KeyError as e:
            # any missing key in dictionary we return raw template...
            _logger.warning('{} msg_dict={} missing key for: "{}"'.format(e, msg_dict, msg))
            formatted_msg = msg
        return formatted_msg

    def _is_bot_pinged(self, values):
        odoobot_id = self.env['ir.model.data'].xmlid_to_res_id("base.partner_root")
        return (4, odoobot_id) in values.get('partner_ids', [])

    def _is_bot_in_private_channel(self, record, bot_xmlid = "base.partner_root", bot_id= None):
        bot_id = bot_id or self.env['ir.model.data'].xmlid_to_res_id(bot_xmlid)
        if record._name == 'mail.channel' and record.channel_type == 'chat':
            return bot_id in record.with_context(active_test=False).channel_partner_ids.ids
        return False


    def get_all_bot_ids(self):
        return [self.env['ir.model.data'].xmlid_to_res_id(xmlid)
                for xmlid in map(lambda o: o['bot'] + "_res_partner" if o['bot'] else "base.partner_root",
                                 self.env['aroolla_base_bot.aroolla_bot_scenario'].read_group([], fields=['bot'],
                                                                                              groupby='bot'))]


    def get_aroolla_partners(self, channel):
        data = {}
        if not hasattr(channel,'channel_partner_ids'): #mail.channel not mail.thread see mail_bot/model_mail_thread.py
            return data
        channel_partners = channel.sudo().with_context({
            'active_test': False}).channel_partner_ids
        if (len(channel_partners)==2):
            all_bot_ids = self.get_all_bot_ids()
            if channel_partners[0].id in all_bot_ids:
                data = {'aroolla_bot_id': channel_partners[0].id,
                        'aroolla_player_id': channel_partners[1].id}
            elif channel_partners[1].id in all_bot_ids:
                data = {'aroolla_bot_id': channel_partners[1].id,
                        'aroolla_player_id': channel_partners[0].id}
        return data

    def global_command_verify(self, record, body, values, command, channel_usr_scn_state):
        """
        checking hard coded escape commands valid in all scenarios
        starting with a '\'

        :param record:
        :param body: body of the message sent by user
        :param values: in/out parameters
        :param command:
        :param channel_usr_scn_state:
        :return: message to send back to user if a global command was handled
        """
        if '\\s' in body:
            # start scenario command:
            # pattern: \s scenario_name
            words = body.split(' ')
            if len(words) > 1:
                scenario_to_start = words[1]
                states = self.env['aroolla_base_bot.usr_scn_state'].create_more_for_users(
                    [self.env.user], 1, scenario_domain=[('name', '=', scenario_to_start)])
                if states:
                    return "successfully started scenario {}".format(scenario_to_start)
                else:
                    return "couldn't start scenario {}".format(scenario_to_start)
            else:
                return "to start a scenario try with: '\\s scenario_name'"
        return
    def _apply_logic(self, record, values, command=None):
        """ Override _apply_logic to allow bot answer for non admin users
        """
        if record and 'aroolla_player_id' not in values:
           aroolla_bot_and_player=self.get_aroolla_partners(record)
           if not aroolla_bot_and_player:
               _logger.warning('can\'t find aroolla_player_id apply_logic called in mail_thread context')
               return super(MailBot, self)._apply_logic(record, values)
           values.update(aroolla_bot_and_player)

        bot_id = values.get('aroolla_bot_id', self.env['ir.model.data'].xmlid_to_res_id("base.partner_root"))
        all_bot_ids = self.get_all_bot_ids()
        if len(record) != 1 or values.get("author_id") in all_bot_ids:
            return
        if self._is_bot_pinged(values) or self._is_bot_in_private_channel(record, bot_id=bot_id):
            body = values.get("body", "").replace(u'\xa0', u' ').strip().lower().strip(".?!")
            answer = self._get_answer(record, body, values, command)
            if answer:
                message_type = values.get('message_type', 'comment')
                subtype_id = values.get('subtype_id', self.env['ir.model.data'].xmlid_to_res_id('mail.mt_comment'))
                _logger.debug('_apply_logic {} message_post => {}'.format(bot_id, answer))
                record.with_context({"mail_create_nosubscribe": True}).sudo().message_post(body=answer,
                                                                                           author_id=bot_id,
                                                                                           message_type=message_type,
                                                                                           subtype_id=subtype_id)


    def _get_answer(self, record, body, values, command):
        """
        Dynamic _get_answer implementation using aroolla_bot_answer model

        :return: text answer to the user
        """

        def goto_state_tour(target_state, target_tour, values=None):
            if target_state:
                _logger.info('goto_state: {}'.format(target_state))
                channel_usr_scn_state.bot_state = target_state
            if target_tour:
                _logger.info('goto_tour: {} values:{}'.format(target_tour, values))
                tour, step = target_tour.split(':')
                if values:
                    # _apply_logic -> get_answer called from aroolla_tour on set_step event
                    # we use values dict to pass back tour_cmd to aroolla_tour and front end (aroolla_tour_manager.js)
                    values['aroolla_bot_tour_cmd'] = {'tour':tour, 'step':int(step)}
                else:
                    # post command to front end (TourManager)
                    tour_cmd = {'aroolla_bot_tour_cmd': {'tour': tour, 'step': int(step)}}
                    self.send_message_to_tour(json.dumps(tour_cmd))

        channel_usr_scn_state = record.get_usr_scn_state(values)
        if not channel_usr_scn_state:
            return
        bot_state = channel_usr_scn_state.bot_state
        bot_scenario = channel_usr_scn_state.scenario

        _logger.debug('aroollabot_scenario_state: {} - {} | RECORD {} | BODY {} | VALUE {} | COMMAND {}'.format(bot_scenario.name, bot_state, record, body, values, command))

        # checking hard coded escape commands valid in all scenarios
        global_command_message = self.global_command_verify(record, body, values, command, channel_usr_scn_state)
        if global_command_message:
            # has priority over command in aroolla_bot_anwser, direct bot reply:
            return global_command_message

        if (not bot_scenario.bot and self._is_bot_in_private_channel(record)) or self._is_bot_in_private_channel(record, bot_scenario.bot + "_res_partner"):
            current_state_answers = self.env['aroolla_base_bot.aroolla_bot_answer'].search(
                ['|', ('state', '=', '*'),('state', '=', bot_state),('scenario_id','=',bot_scenario.id)],
                order='sequence asc')
            for a in current_state_answers:
                if a.timeout_trigger:
                    time_since_state_change = (datetime.now() - channel_usr_scn_state.write_date).seconds
                    if time_since_state_change > random.randint(a.timeout_trigger, 2*a.timeout_trigger):
                        _logger.info('{} bot_answer#{} timeout_trigger {}s'.format(command,a.id,time_since_state_change))
                        goto_state_tour(a.tmo_state, a.tmo_tour, values if command == 'check_tour' else None)
                        return a.tmo_answer
                matched = a.self_trigger
                if a.self_trigger:
                    _logger.info('{} bot_answer#{} self_trigger'.format(command, a.id))
                if command == 'check_tour':
                    # apply bot logic on tour event
                    current_tour = values['tour']
                    matched =  matched or a.tour_trigger == current_tour
                    if matched:
                        _logger.info('check_tour("{}") matched bot_answer#{}'.format(current_tour,a.id))
                    else:
                        continue
                elif command == 'check_model':
                    # apply bot logic on model event
                    # in model notification context only model_trigger is handled
                    # self_trigger is ignored to avoid recursive calls to verif_fn updating some watched model
                    matched = a.model_trigger.model == values['model']
                    if matched:
                        _logger.info('check_model("{}") matched bot_answer#{}'.format(a.model_trigger.model, a.id))
                elif command == 'bot_control':
                    # for now bot_control only match on self_trigger or timeout_trigger (common case above)
                    if matched:
                        _logger.info('bot_control matched bot_answer#{}'.format(a.id))
                elif command in (None,'start_dialog','help'):
                    # apply bot logic on user message
                    if not matched:
                        if a.regex == False and a.tour_trigger == False:
                            _logger.info(
                                '{} bot_answer#{} has no regex nor tour_trigger body="{}" => not matched'.format(
                                    command, a.id, body))
                        else:
                            if a.regex and re.search(a.regex, body):
                                matched = True
                                _logger.info(
                                    '{} bot_answer#{} regex=({}) body="{}" matched'.format(
                                        command, a.id,a.regex, body))
                else:
                    _logger.warning('unknown command: {} for bot_answer#{}'.format(command, a.id))
                if matched:
                    if a.verify:
                        verified = False
                        try:
                            # a.verify can be an json encoded Odoo domain
                            verify = json.loads(a.verify)
                            verify_results = self.env[verify['model']].search(verify['domain'])
                            verified = len(verify_results) > 0
                        except JSONDecodeError:
                            # or a verification method name
                            verif_fn = getattr(self.env['aroolla_base_bot.aroolla_bot_action'], a.verify, False)
                            
                            if verif_fn and callable(verif_fn):
                                if a.template_id:
                                    verified = verif_fn(record, body, values, command, channel_usr_scn_state, a.template_id)
                                else:
                                    verified = verif_fn(record, body, values, command, channel_usr_scn_state)
                            else:
                                _logger.warning(
                                    'INVALID verification( {} ) function. It does not exist or is not callable'.format(
                                        a.verify))
                                # invalid verify here we try next record for current_state if any
                                continue
                        if verified is None:
                            _logger.info('verification( {} ) None'.format(a.verify))
                            goto_state_tour(a.none_state, a.none_tour, values if command == 'check_tour' else None)
                            if a.none_answer:
                                return self.format_msg(a.none_answer, values)
                        elif not verified:
                            # failure
                            _logger.info('verification( {} ) failed'.format(a.verify))
                            goto_state_tour(a.err_state, a.err_tour, values if command == 'check_tour' else None)
                            if a.err_answer:
                                return self.format_msg(a.err_answer, values)
                        else:
                            _logger.info('verification( {} ) succeeded'.format(a.verify))

                    # bot_command from verify function
                    if 'bot_command' in values:
                        # post command to front end (TourManager)
                        self.send_message_to_tour(json.dumps(values))
                        goto_state_tour(a.ok_state, None)
                        # also return answer to the user through chat (msg is dynamically generated answer by verif_fn)
                        return self.format_msg(values['bot_command']['msg'], values) if 'msg' in values['bot_command'] else None

                    goto_state_tour(a.ok_state, a.ok_tour, values if command == 'check_tour' else None)
                    if a.ok_answer:
                        return self.format_msg(a.ok_answer, values)
            if command in ['check_tour','check_model']:
                # don't talk to user if they dont ask for and no event
                return
            if a.scenario_id and a.scenario_id.name=='ttfn' and a.state=='done':
               return
        return super(MailBot, self)._get_answer(record, body, values, command)

