# -*- coding: utf-8 -*-
# Part of Aroolla. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _

class AroollaBotAction(models.AbstractModel):
    _name = 'aroolla_base_bot.aroolla_bot_action'
    _description = 'Aroolla Bot Action'

    @classmethod
    def add_bot_command(cls, values, msg=None, tour=None, location=None):
        """
        :param values: in/out value dict
        :param msg: bot message
        :param tour: combined target tour:step
        :param location: target location url
        :return: updated values with expected format dict with split tour/step
        """
        tour_name, step = tour.split(':') if tour else (None, None)
        if tour or location:
            values['aroolla_bot_tour_cmd'] = {'tour': tour_name, 'step': None if step is None else int(step), 'location': location}
        values['bot_command'] = {'msg': _(msg)}

    def add_bot_msg_dict(self, values,  **kwargs):
        values['bot_msg_dict'] = values.get('bot_msg_dict', {})
        values['bot_msg_dict'].update(**kwargs)
