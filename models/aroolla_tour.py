# -*- coding: utf-8 -*-
# Part of Aroolla. See LICENSE file for full copyright and licensing details.

import logging
from datetime import datetime

from odoo import api, models, fields, _

_logger = logging.getLogger(__name__)


class AroollaTour(models.Model):
    _name = 'aroolla_base_bot.tour'
    _description = 'Aroolla Tour'

    user_id = fields.Many2one('res.users', string='Consumed by')
    name = fields.Char(string="Tour name", required=True)
    step = fields.Integer(default=1, help="Current step of the tour")
    state = fields.Selection([('A', 'Activated'), ('C', 'Consumed')], default='A')
    current_game_duration = fields.Datetime(related='user_id.game_duration', store=False)
    game_duration = fields.Char(compute='_compute_duration', store=False)
    game_start_session = fields.Datetime(related='user_id.game_start_session', store=False)
    game_finished = fields.Boolean(related='user_id.game_finished', store=False)
    game_penalties = fields.Integer(related='user_id.game_penalties', store=False)

    @api.depends('current_game_duration', 'game_start_session', 'game_finished')
    def _compute_duration(self):
        for rec in self:
            if rec.game_finished:
                # Game is finished, game_duration is final
                game_duration = rec.current_game_duration - datetime(2010, 1, 1, 0, 0, 0, 0)
                rec.game_duration = str(game_duration).split('.')[0]
            else:
                if not rec.game_start_session:
                    # Game is not finished, game_duration is current
                    if rec.current_game_duration:
                        game_duration = rec.current_game_duration - datetime(2010, 1, 1, 0, 0, 0, 0)
                        rec.game_duration = str(game_duration).split('.')[0]
                    else:
                        rec.game_duration = "0:00:00"
                else:
                    # Game is running, we need to add duration to current session
                    current_session = datetime.now() - rec.game_start_session
                    if rec.current_game_duration:
                        game_duration = rec.current_game_duration + current_session
                        game_duration -= datetime(2010, 1, 1, 0, 0, 0, 0)
                        rec.game_duration = str(game_duration).split('.')[0]
                    else:
                        rec.game_duration = str(current_session).split('.')[0]

    @api.model
    def set_step(self, tour_names, steps, states):
        """
         Records given tours positions for current user
        :param tour_names: list of tour names to modify
        :param steps: list of corresponding steps
        :param steps: list of corresponding states (A/C)
        :return: new step if need to change
        """
        for name, step, state in zip(tour_names, steps, states):
            if name in ('roasteria_solo_oexp'):
                vals = {'name': name, 'user_id': self.env.uid, 'step': step, 'state': state}
                rset = self.search([('name','=',name),('user_id','=',self.env.uid)])
                _logger.debug('set_step: {}:{}:{}'.format(name, step, state))
                if not len(rset):
                    self.create(vals)
                else:
                    rset.write(vals)
        aroolla_bot_tour_cmd = None
        for bot in self.env['mail.channel'].get_aroollabots():
            values = {'tour': '{}:{}:{}'.format(name, step, state),
                      'aroolla_bot_id': bot['aroolla_bot_id'],
                      'aroolla_player_id': bot['aroolla_player_id']}
            if bot['channel']:
                self.env['mail.bot']._apply_logic(bot['channel'], values, command="check_tour")
            else:
                _logger.warning('No bot_channel on set_step cannot apply logic')
            aroolla_bot_tour_cmd = values.get('aroolla_bot_tour_cmd', None)
            if aroolla_bot_tour_cmd:
                # for now we just break when bot recognized (acted on) the tour
                # todo: filter list of channel called based on tour / concerned scenario
                break
        return dict(aroolla_bot_tour_cmd=aroolla_bot_tour_cmd) if aroolla_bot_tour_cmd else None
