# -*- coding: utf-8 -*-
# Part of Aroolla. See LICENSE file for full copyright and licensing details.

from odoo import models, fields


class Users(models.Model):
    _inherit = 'res.users'
    odoobot_state = fields.Selection(
        selection_add=[
            ('playing_aroolla', 'Playing Aroolla'),
        ])
    aroollabot_states = fields.One2many(comodel_name='aroolla_base_bot.usr_scn_state', inverse_name='user', string='Per scenario states of the user')
    game_duration = fields.Datetime('Game duration', readonly=True)
    game_penalties = fields.Integer('Game penalties', readonly=True, default=0)
    game_start_session = fields.Datetime('Game start session', readonly=True)
    game_finished = fields.Boolean('Game finished', default=False, readonly=True)

    # worker/job related fields
    worker_id = fields.Many2one('aroolla_base_bot.worker', default=None)
    start_date = fields.Datetime(string='Last worker start_date', readonly=True)
    end_date = fields.Datetime(string='Last worker end_date', readonly=True)
