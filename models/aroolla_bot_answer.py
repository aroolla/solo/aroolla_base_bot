# -*- coding: utf-8 -*-
# Part of Aroolla. See LICENSE file for full copyright and licensing details.
import random

import logging
from odoo import api, models, fields, _

_logger = logging.getLogger(__name__)

class AroollaBotAnswer(models.Model):
    _name = 'aroolla_base_bot.aroolla_bot_answer'
    _description = 'Aroolla Bot Answer'

    # initial state of the BotAnswer
    scenario_id = fields.Many2one('aroolla_base_bot.aroolla_bot_scenario', 'Scenario')
    state = fields.Char(help='State activating this Bot Answer')
    sequence = fields.Integer('Sequence', default=1, help="Order of the records having the same state")

    # conditions
    regex = fields.Char(help="Regular expression to test user response text body. Triggering verification/ok answer if matched, error answer if not")
    tour_trigger = fields.Char(help='Tour state triggering verification/answer: tour_name[:step]')
    model_trigger = fields.Many2one('ir.model', 'Model trigger')
    timeout_trigger = fields.Integer(help='Timeout in seconds')
    self_trigger = fields.Boolean(help='Trigger the action when the state change', default=False)

    # verification
    verify = fields.Char(help='Verification method name or json domain expression')

    # On ok targets
    ok_answer = fields.Char(help='Bot Answer on verification success ', translate=True)
    ok_state = fields.Char(help='Target Bot state on verification success')
    ok_tour = fields.Char(help='Tour target on verification success: tour_name[:step]')

    # On error targets
    err_answer = fields.Char(help='Bot Answer on verification error', translate=True)
    err_state = fields.Char(help='Target Bot state on verification error')
    err_tour = fields.Char(help='Tour target on verification error: tour_name[:step]')

    # On None targets
    none_answer = fields.Char(help='Bot Answer when verification returns None', translate=True)
    none_state = fields.Char(help='Target Bot state when verification returns None')
    none_tour = fields.Char(help='Tour target when verification returns None: tour_name[:step]')

    # On timeout targets
    tmo_answer = fields.Char(help='Bot Answer on timeout', translate=True)
    tmo_state = fields.Char(help='Target Bot state on timeout')
    tmo_tour = fields.Char(help='Tour target on timeout: tour_name[:step]')

    # Template id for verify function

    template_id = fields.Char(help='A product or partner id that a corresponding verify function will work on', default='')

    @api.multi
    def name_get(self):
        return [(answer.id,
                 '{}/{}/({},)-{}- =>{}'.format(answer.state, answer.regex, answer.id, answer.verify, answer.ok_state))
                for answer in self]


class AroollaBotScenario(models.Model):
    _name = 'aroolla_base_bot.aroolla_bot_scenario'
    _description = 'Aroolla Bot Scenario'

    name = fields.Char(help='Name of the scenario')
    sequence = fields.Integer('Sequence', default=1, help="Order of the scenarios")
    type = fields.Selection([
        ('tour', 'Tour'),
        ('business', 'Business'),
        ('congrats', 'Congrats')], 'Scenario Type',
        default='tour', required=True)
    bot = fields.Char(help='XML_ID of the bot/user')
    score = fields.Integer(help='Score when completed')


class AroollaUserScenarioState(models.Model):
    _name = 'aroolla_base_bot.usr_scn_state'
    _table = 'abb_auss' #default table name too long with Many2many relations (combined table name lenght limit)
    _description = 'Aroolla User Scenario State'

    user = fields.Many2one(comodel_name='res.users')
    scenario = fields.Many2one(comodel_name='aroolla_base_bot.aroolla_bot_scenario')
    bot_state = fields.Char(string="Aroolla Bot State", readonly=False, required=True, default="welcome")
    point = fields.Integer(default=0, help="Point of the scenarios")
    # _sql_constraints = [('uniq_usr_scn', 'UNIQUE (user, scenario)',
    #                      'There can only be a single state per scenario for a given user')]

    @api.multi
    def name_get(self):
        return [(ust.id, 'user_scenario_state({},)_{}_{}_{}'.format(ust.ids, ust.user, ust.scenario, ust.bot_state)) for
                ust in self]

    @api.model
    def create_all_for_users(self, user_list):
        data_list = []
        for scenario in self.env['aroolla_base_bot.aroolla_bot_scenario'].search([]):
            initial_bot_answer = self.env['aroolla_base_bot.aroolla_bot_answer'].search(
                [('scenario_id', '=', scenario.id)], order='sequence asc', limit=1)
            if len(initial_bot_answer):
                # scenario implemented
                for user in user_list:
                    data_list.append(dict(user=user.id, scenario=scenario.id, bot_state=initial_bot_answer.state, point=scenario.score))
        if len(data_list):
            return self.create(data_list)

    @api.model
    def create_more_for_users(self, user_list, nbo_scenario, scenario_domain=None):
        if scenario_domain is None:
            scenario_domain = []
        data_list = []
        all_scenarios = self.env['aroolla_base_bot.aroolla_bot_scenario'].search(scenario_domain, order='sequence asc')
        scenario_first_states = {}
        for scenario in all_scenarios:
            initial_bot_answer = self.env['aroolla_base_bot.aroolla_bot_answer'].search(
                [('scenario_id', '=', scenario.id)], order='sequence asc', limit=1)
            if len(initial_bot_answer):
                scenario_first_states[scenario] = initial_bot_answer.state
        implemented_scenarios = all_scenarios.filtered(lambda s: s in scenario_first_states)
        _logger.debug('usr_scn_state.create_more_for_users({}, {}, {})'.format(user_list, nbo_scenario, scenario_domain))
        for user in user_list:
            already_active_scenarios = user.aroollabot_states.mapped('scenario')
            remaining_scenarios = implemented_scenarios - already_active_scenarios
            # avoid starting multiple scenarios for the same bot at the same time
            active_business_states = user.aroollabot_states.filtered(
                lambda r: r.scenario.type == 'business' and r.bot_state != 'done')
            active_bots = active_business_states.mapped('scenario').mapped('bot')
            if active_bots:
                remaining_scenarios = remaining_scenarios.filtered(lambda s: s.bot not in active_bots)
            remaining_scenarios = list(remaining_scenarios)
            random.shuffle(remaining_scenarios)
            for index, scenario in enumerate(remaining_scenarios):
                if index < nbo_scenario:
                    data_list.append(
                        dict(user=user.id, scenario=scenario.id, bot_state=scenario_first_states[scenario], point=scenario.score))
        _logger.debug('usr_scn_state.create_more_for_users({}, {}, {}) => {}'.format(user_list, nbo_scenario, scenario_domain, data_list))
        if len(data_list):
            return self.create(data_list)
