# -*- coding: utf-8 -*-
# Part of Aroolla. See LICENSE file for full copyright and licensing details.

import logging

from odoo import api, models, _

_logger = logging.getLogger(__name__)

class Channel(models.Model):
    _inherit = 'mail.channel'

    def _execute_command_help(self, **kwargs):
        super(Channel, self)._execute_command_help(**kwargs)
        self.env['mail.bot']._apply_logic(self, kwargs, command="help")  # kwargs are not usefull but...

    @api.model
    def channel_fetch_listeners(self, uuid):
        """ Return the id, name and email of partners listening to the given channel """
        result = super().channel_fetch_listeners(uuid)
        odoobot = self.env.ref("base.partner_root").sudo()
        return result + [{'id': odoobot.id, 'name': odoobot.name, 'email': odoobot.email}]

    @api.model
    def init_odoobot(self,bot=None):
        user = self.env.user.sudo()
        if bot or user.odoobot_state == 'not_initialized':
            partner = user.partner_id
            bot = bot or self.env['ir.model.data'].xmlid_to_object("base.partner_root")
            channel_name =  bot.name if bot else 'OdooBot'
            channel = self.sudo().with_context({"mail_create_nosubscribe": True}).create({
                'channel_partner_ids': [(4, partner.id), (4, bot.id)],
                'public': 'private',
                'channel_type': 'chat',
                'email_send': False,
                'name': channel_name
            })
            _logger.debug(
                'init_odoobot({}) as {} created {} [{}] ({})'.format(bot, user, channel, channel.name, channel.channel_partner_ids))
            if channel_name == 'OdooBot':
                user.odoobot_state = 'playing_aroolla'
                # todo verify remove this first apply_logic when called from get_odoobot
                self.env['mail.bot']._apply_logic(channel, {'author_id':partner.id, 'partner_ids':[(4, partner.id), (4, bot.id)]}, command="start_dialog")
            return channel

    @api.model
    def get_aroollabots(self, values = None):

        if self.env.user.id != 1:
            user = self.env.user
            partner = self.env.user.partner_id
        else:
            # in sudo() context we need to find current user from channel
            if values and 'aroolla_player_id' in values:
                # aroolla_player_id must be passed in values in sudo context
                partner = self.env['res.partner'].browse(values.get('aroolla_player_id'))
                user = partner.user_ids[0]
            else:
                # no user context to find bot state in
                _logger.debug('no user context passed - returning no bot')
                return []
        bots = []
        for scenario_state in user.aroollabot_states:
            if scenario_state.scenario.bot:
                bot_user = self.env['ir.model.data'].xmlid_to_object(scenario_state.scenario.bot)
                bot = bot_user.sudo().partner_id
                channel_name = bot_user.sudo().name
                if scenario_state.scenario.type == 'business' and scenario_state.bot_state == 'done':
                    # we don't return bot channel with only terminated business scenario
                    continue
            else:
                bot = self.env['ir.model.data'].xmlid_to_object("base.partner_root")
                channel_name = 'OdooBot'
            channel_already_exists = False
            for channel in self.search([('public', '=', 'private'),
                                        ('channel_type', '=', 'chat'), ('name', '=', channel_name)]):
                if set((partner.id, bot.id)) == set(
                        # odoobot partner is an inactive record (active=False so only seen with_context: active_test=False
                        channel.sudo().with_context({
                            'active_test': False}).channel_partner_ids.ids):  # or partner.id in channel.channel_partner_ids.ids:
                    channel_already_exists = True
                    break
            if channel_already_exists:
                bots.append({'channel': channel, 'aroolla_player_id': partner.id, 'aroolla_bot_id': bot.id})
            else:
                # was self.sudo() before but then we definitely loose self.env.user
                channel = self.init_odoobot(None if channel_name == 'OdooBot' else bot)
                if channel:
                    bots.append({'channel': channel, 'aroolla_player_id': partner.id, 'aroolla_bot_id': bot.id})
        return bots

    @api.multi
    def get_usr_scn_state_list(self, values):
        channel_partners = set(self.sudo().with_context({
                            'active_test': False}).channel_partner_ids)
        if self.env.user.id != 1:
            partner = self.env.user.partner_id
            user = self.env.user
        else:
            partner = self.env['res.partner'].browse(values.get('aroolla_player_id'))
            user = partner.user_ids[0]
        bot_partner = list((channel_partners - set(partner)))[0]
        bot_partner_xmlid = bot_partner.get_external_id()[bot_partner.id]
        bot_user = bot_partner.user_ids[0]
        bot_user_xmlid = bot_user.get_external_id()[bot_user.id]
        # Find the user active scenarios handled by this bot
        if bot_partner_xmlid in ['', 'base.partner_root']:
            # filter scnenario based on tour type endstate  for unspecified bot or odoobot
            bot_scenarios = user.aroollabot_states.filtered(
                lambda s: (not s.scenario.bot or s.scenario.bot == 'base.user_root') and s.bot_state != 'tour_done')
        else:
            bot_scenarios = user.aroollabot_states.filtered(
                lambda s: s.scenario.bot == bot_user_xmlid and s.bot_state != 'done')
        return bot_scenarios

    @api.multi
    def get_usr_scn_state(self, values):
        scenario = self.get_usr_scn_state_list(values)
        # if not scenario:
        #     self.env['aroolla_base_bot.aroolla_bot_scenario'].search('')
        return scenario
