import time
import logging
import threading
import os
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DATETIME_FORMAT

import psycopg2
from psycopg2.extensions import AsIs

from psycopg2.extensions import ISOLATION_LEVEL_READ_COMMITTED, ISOLATION_LEVEL_AUTOCOMMIT
from contextlib import closing, contextmanager

import odoo
from odoo.http import request
from odoo import api, models, fields, _


_logger = logging.getLogger(__name__)

class AlreadyLocked(Exception):
    """ when postgres select for update fails """
    pass

class Worker(models.Model):
    _description = 'Aroolla Base Bot Worker'
    _name = 'aroolla_base_bot.worker'

    type = fields.Selection((('thread', 'thread'), ('process', 'process')))
    os_id = fields.Char(compute="_value_os_id", default=None, help="combined worker os id", store=True, readonly=True)
    hid = fields.Char(default=None, help="Host name")
    pid = fields.Char(default=None, help="Process id")
    tid = fields.Char(default=None, help="Thread id")
    jobs = fields.One2many(comodel_name='res.users', inverse_name='worker_id', string='Jobs(users)')

    start_date = fields.Datetime(string='Worker start Date', default=fields.Datetime.now(), readonly=True)
    end_date = fields.Datetime(string='Worker end Date', readonly=True)
    exec_duration = fields.Char(compute="_value_exec_duration", default=None, help="Worker execution time")

    @api.multi
    def name_get(self):
        return [(worker.id, 'w({},)_{}'.format(worker.id, worker.os_id)) for worker in
                self]

    @api.depends('hid', 'pid', 'tid')
    def _value_os_id(self):
        for record in self:
            record.os_id = '{}:p#{} t#{}'.format(record.hid, record.pid, record.tid)

    @api.depends('start_date', 'end_date')
    def _value_exec_duration(self):
        for record in self:
            worker_summary = ''
            if not record.start_date:
                record.exec_duration = worker_summary+':---'
            else:
                end_date = record.end_date
                if record.end_date:
                    record.exec_duration = '{}:{}'.format(worker_summary, str(end_date - record.start_date)[2:9])
                else:
                    record.exec_duration = '{}:...'.format(worker_summary)


    def locked_jobs(self, jobs, job_table=None):
        """
        generator locking jobs for worker
        :param jobs: list of available jobs
        :param job_table: optional name of table associated with job object.
               default to table associated with the corresponding Odoo model
        :returns yields locked jobs
        """

        db_name = threading.current_thread().dbname
        db = odoo.sql_db.db_connect(db_name)
        for job in jobs:
            # read all job record properties now to avoid serialization access issue while holding the lock
            job_table = job_table or job._table
            job_id, job_display_name = job.id, job.display_name
            lock_cr = db.cursor()
            lock_cr._cnx.set_isolation_level(ISOLATION_LEVEL_READ_COMMITTED)
            try:
                lock_cr.execute("""SELECT *
                                   FROM %s
                                   WHERE id=%s AND worker_id IS null
                                   FOR UPDATE NOWAIT""",
                                (AsIs(job_table), job_id), log_exceptions=False)
                locked_job = lock_cr.fetchone()
                if not locked_job:
                    _logger.debug('{} no record {}->{}` already executed by {}. skipping it'.format(job, job_display_name,  self.display_name, job.worker_id.display_name))
                    continue
                _logger.debug('{}->{} locked by {}'.format(job, job_display_name, self.display_name))

                lock_cr.execute("""UPDATE %s
                                   SET worker_id=%s,start_date=%s
                                   WHERE id=%s""", (AsIs(job_table), self.id, fields.Datetime.to_string(datetime.now()), job_id))
                lock_cr.commit() # commit the update and unlock the record
                _logger.debug('{}->{} unlocked by {}'.format(self, job_display_name, self.display_name))

                yield job

                lock_cr.execute("UPDATE %s SET end_date=%s, worker_id=null WHERE id=%s", (AsIs(job_table), fields.Datetime.to_string(datetime.now()), job_id))
                lock_cr.commit()
                _logger.debug('{}->{} finished by {}'.format(self, job_display_name, self.display_name))
            except psycopg2.OperationalError as e:
                _logger.debug(
                    '{} {}->{}` already locked by {} skipping it.'.format(self.display_name, self,
                                                                                       job_display_name,
                                                                                       job.worker_id.display_name))
                continue
            finally:
                lock_cr.close()
        else:
            yield from () # we'll at least, and in the end, "yield nothing" (in case all jobs are already assigned)

    @api.model
    def stop_workers(self):
        # Interrupts all current running threads on all hosts and processes
        AroollaThread.notify_all(self.env, 'exit')
        # prevent new threads being launched by remaining cron actions by disabling our default event (see aroolla_bot_control main_action)
        solo_event = self.env.ref('aroolla_game_roasteria_solo_data.event_event_1')
        solo_event.active = False
        self.env.cr.commit()
        time.sleep(.2)
        # prevent new thread launch by disabling our cron actions
        bot_control = self.env.ref('aroolla_base_bot.model_aroolla_base_bot_aroolla_bot_control')
        bot_control_scheduled_actions = self.env['ir.cron'].search([('model_id', '=', bot_control.id),('active','=', True)])
        while len(bot_control_scheduled_actions) and not bot_control_scheduled_actions.try_write({'active':False}):
            _logger.debug('stop_workers - {} active cron actions remaining. retrying in .2s'.format(len(bot_control_scheduled_actions)))
            time.sleep(.2)
            self.env.cr.commit()
            bot_control_scheduled_actions = self.env['ir.cron'].search([('model_id', '=', bot_control.id),('active','=', True)])

    @api.model
    def start_workers(self):
        # re-activate event
        solo_event = self.env.ref('aroolla_game_roasteria_solo_data.event_event_1')
        if not solo_event.active:
            # Cleanup locked users if any
            locked_users = self.env['res.users'].search([('worker_id','!=',None)])
            for user in locked_users:
                _logger.debug(
                    'start_workers unlock {} from {} terminated at {}'.format(user, user.worker_id.display_name,
                                                                              user.worker_id.end_date))
                if user.worker_id.end_date:
                    user.worker_id = None
            solo_event.active = True
        # Re-enables bot_control cron actions
        bot_control = self.env.ref('aroolla_base_bot.model_aroolla_base_bot_aroolla_bot_control')
        bot_control_scheduled_actions = self.env['ir.cron'].search([('model_id', '=', bot_control.id),
                                                                    ('active', '=', False)])
        if len(bot_control_scheduled_actions):
            bot_control_scheduled_actions.write({'active':True})

class Job(models.Model):
    _description = 'Aroolla Base Bot Job'
    _name = 'aroolla_base_bot.job'

    worker_id = fields.Many2one('aroolla_base_bot.worker', default=None)
    start_date = fields.Datetime(string='Job start Date', readonly=True)
    end_date = fields.Datetime(string='Job end Date', readonly=True)
    exec_duration = fields.Char(compute="_value_exec_duration", default=None, help="Job execution time")

    # generic job args - not used here yet
    # args = fields.Text(required=False, default=None)
    # kwargs = fields.Text(required=False, default=None)

    # aroolla_bot specific arg
    user = fields.Many2one('res.users', default=None)

    @api.multi
    def name_get(self):
        return [(job.id, '{}({},)_{}'.format(job._name, job.id, job.user.login)) for job in self]

    @api.depends('start_date', 'end_date')
    def _value_exec_duration(self):
        for record in self:
            # fns = json.loads(record.sim_function_name)
            # job_summary = '-'.join([fn[4:7] for fn in fns])
            job_summary = '{}({})'.format(record.user.name, record.user.login)
            if not record.start_date:
                record.exec_duration = job_summary+':---'
            else:
                end_date = record.end_date
                if record.end_date:
                    record.exec_duration = '{}:{}'.format(job_summary, str(end_date - record.start_date)[2:9])
                else:
                    record.exec_duration = '{}:...'.format(job_summary)



class AroollaThread():

    MAIN_JOB_PERIOD = 10

    def start_thread(self, on_server_startup=False, limit_time=None):
        current_thread = threading.current_thread()
        thr_name = get_full_thread_name(current_thread)
        _logger.info('{} start_thread(on_server_startup={})'.format(thr_name, on_server_startup))

        def is_cron_worker():
            # we check if there is a request handled by the caller (in current env)
            try:
                p = request.params
            except (AttributeError, RuntimeError) as e:
                return True
            else:
                # request params is present so this we're called from an HttpWorker
                return False

        def get_tls_value(key):
            try:
                return getattr(threading.current_thread(), key)
            except AttributeError:
                return None

        def set_tls_value(key, value):
            setattr(threading.current_thread(), key, value)
            return value

        def get_tls_thread(thr_key):
            thread = get_tls_value(thr_key)
            return thread if thread and thread.is_alive() else None

        cron_worker = is_cron_worker()

        thread_exit_cond = get_tls_value('thread_exit_cond') or set_tls_value('thread_exit_cond', threading.Condition())

        # launching/connecting  to thread_listener
        if on_server_startup:
            thread_listener = get_tls_thread('thread_listener')
            if not thread_listener:
                thread_listener = threading.Thread(target=self.thread_cr_wrapper, daemon=True, name="AroollaExitListener",
                                                   args=(None, self.run_listener, thread_exit_cond))
                current_thread.thread_listener = thread_listener
                thread_listener.start()
                _logger.info('%s start_thread thread_listener started: %s', thr_name,
                             get_full_thread_name(thread_listener))
            else:
                _logger.info('%s start_thread thread_listener already running: %s', thr_name,
                             get_full_thread_name(thread_listener))
            return
        else:
            worker_type = 'cron' if cron_worker else 'http'
            _logger.info('{} called from {} worker'.format(thr_name, worker_type))
            if not cron_worker:
                # http workers do not start any thread in solo for now
                return
            thread_listener = get_tls_thread('thread_listener')
            if not thread_listener:
                subscribe_to = ('exit',) if cron_worker else ('change_day',)
                thread_listener = threading.Thread(target=self.thread_cr_wrapper, daemon=True, name="AroollaExitListener",
                                                   args=(None, self.run_listener, subscribe_to, thread_exit_cond))
                current_thread.thread_listener = thread_listener
                thread_listener.start()
                _logger.info('%s start_thread %s worker thread_listener started: %s', thr_name, worker_type,
                             get_full_thread_name(thread_listener))
            else:
                _logger.info('%s start_thread %s worker thread_listener already running: %s', thr_name,
                             worker_type,
                             get_full_thread_name(thread_listener))

        # launching/connecting to aroolla_thread
        aroolla_thread = get_tls_thread('aroolla_thread')
        if not aroolla_thread:
            aroolla_thread = threading.Thread(target=self.thread_main, daemon=True, name="AroollaThread",
                                                   args=(thread_exit_cond,),
                                                   kwargs={'on_startup': on_server_startup})
            # save thread id in current thread localstorage
            current_thread.aroolla_thread = aroolla_thread
            aroolla_thread.start()
            _logger.info('%s start_thread aroolla_thread started: %s', thr_name,
                         get_full_thread_name(aroolla_thread))
            if cron_worker:
                _logger.info('%s start_thread joining #%d', thr_name,
                             aroolla_thread.ident)
                aroolla_thread.join(timeout=limit_time)

                # gracefully exit worker thread after limit_time to recycle cron process
                if limit_time and aroolla_thread.is_alive():
                    self.stop_thread(thread_exit_cond)
                    retries = 0
                    while aroolla_thread.is_alive() and retries < 3:
                        retries += 1
                        aroolla_thread.join(timeout=10)

                _logger.info('%s start_thread joined #%d (is_alive=%d)', thr_name,
                             aroolla_thread.ident, aroolla_thread.is_alive())
        else:
            _logger.info('%s start_thread ignored aroolla_thread already running :%s', thr_name,
                         get_full_thread_name(aroolla_thread))

    @staticmethod
    def stop_thread(thread_exit_cond = None):
        if thread_exit_cond:
            with thread_exit_cond:
                thread_exit_cond.notify()
    @classmethod
    def get_os_id(cls):
        return '{}:p#{} t#{}'.format(os.uname().nodename, os.getpid(), threading.get_ident())


    @classmethod
    def thread_cr_wrapper(cls, db_name, fn_or_list, *args,**kwargs):
        sim_fn_start = datetime.now()
        if callable(fn_or_list):
            fn_list = [fn_or_list]
        else:
            fn_list = fn_or_list

        if not db_name:
            registries = odoo.modules.registry.Registry.registries
            db_name, registry = next(registries.items().__iter__())
            for retry in range(100):
                if registry.ready:
                    break
                else:
                    time.sleep(3)
            if retry>98:
                # _logger.error('run_simulation %s registry %s not ready - aborting', db_name, registry)
                raise RuntimeError(
                    'thread_cr_wrapper {} func={} db={} registry {} not ready - aborting'.format(cls.get_os_id(),
                                                                                                        fn_list[0].__name__,
                                                                                                        db_name,
                                                                                                        registry))
        db = odoo.sql_db.db_connect(db_name)
        threading.current_thread().dbname = db_name
        with closing(db.cursor()) as thread_cr:
            thread_cr._cnx.set_isolation_level(ISOLATION_LEVEL_READ_COMMITTED)
            with api.Environment.manage():
                env = api.Environment(thread_cr, 1, {})
                duration_str_list = []
                for func in fn_list:
                    try:
                        func(env, *args, **kwargs)
                    except Exception as e:
                        _logger.error(
                            'thread_cr_wrapper %s %s encountered an Exception: %s rolling back',
                            cls.get_os_id(), func.__name__, e, exc_info=True)
                        thread_cr.rollback()
                    else:
                        thread_cr.commit()
                    sim_fn_end = datetime.now()
                    duration_str = '{}:{}'.format(func.__name__[4:7] if func.__name__.startswith('sim_') else func.__name__,
                                                  str(datetime.now() - sim_fn_start)[2:9])
                    duration_str_list.append(duration_str)
                    _logger.info('thread_cr_wrapper %s  %s(): %s -> %s = %s', cls.get_os_id(), func.__name__,
                                 sim_fn_start, sim_fn_end, duration_str)
                    sim_fn_start = datetime.now()
                return {'start_date': sim_fn_start, 'end_date': sim_fn_end,
                        'duration_str': '-'.join(duration_str_list)}

    @classmethod
    def thread_main(cls,exit_condition, on_startup=False):
        # see cron_thread
        if on_startup:
            # fixme waiting (arbitrary 3s) for registry to be ready
            time.sleep(3)
        registries = odoo.modules.registry.Registry.registries
        for db_name, registry in registries.items():
            _logger.info('thread_main for db_name={}'.format(db_name))
            if registry.ready:
                thread_cr = None
                active_one = None
                try:
                    #see ir_cron._acquire_job -> _process_jobs
                    db = odoo.sql_db.db_connect(db_name)
                    threading.current_thread().dbname = db_name
                    thread_cr = db.cursor()
                    thread_cr._cnx.set_isolation_level(ISOLATION_LEVEL_READ_COMMITTED)
                    with api.Environment.manage():
                        env = api.Environment(thread_cr, 1, {})
                        l=1

                        # if running_one and len(running_one) == 1:
                        worker = env['aroolla_base_bot.worker'].create(
                            dict(hid=os.uname().nodename, pid=os.getpid(), tid=threading.get_ident(),
                                 type='process', start_date = fields.Datetime.now()) )
                        env.cr.commit()

                        # completed_current_day = False
                        with exit_condition:
                            while True:
                                loop_start = datetime.now()
                                try:
                                    cls.main_job(env, worker)
                                    loop_end = datetime.now()
                                    duration = loop_end - loop_start
                                    _logger.info('{} done with  loop#{} in {}s'.format(worker.display_name, l, duration.total_seconds()))
                                    thread_cr.commit()
                                except Exception as e:
                                    loop_end = datetime.now()
                                    duration = loop_end - loop_start
                                    _logger.error(
                                        '{}  inner loop encountered an Exception after {}s: {}'.format(worker.display_name, duration.total_seconds(), e), exc_info=True)
                                else:
                                    env.cr.commit()
                                if duration.total_seconds() < cls.MAIN_JOB_PERIOD:
                                    next_loop_timeout = cls.MAIN_JOB_PERIOD - duration.total_seconds()
                                else:
                                    next_loop_timeout = 0
                                _logger.info('{} next_loop_timeout={}s => {}'.format(worker.display_name, next_loop_timeout,(datetime.now()+timedelta(seconds=next_loop_timeout)).strftime(DATETIME_FORMAT)))
                                # termination check
                                if exit_condition.wait(timeout=next_loop_timeout):
                                    _logger.info('{} aroolla thread terminating'.format(worker.display_name))
                                    worker.end_date = fields.Datetime.now()
                                    env.cr.commit()
                                    break
                                _logger.info('{} exit_condition timed out at {}'.format(worker.display_name, datetime.now().strftime(DATETIME_FORMAT)))
                                l += 1
                except Exception as e:
                    _logger.error('{} thread_main{} encountered an Exception: {}'.format(cls.get_os_id(),
                                                                                            active_one.id if active_one else 0,
                                                                                            e), exc_info=True)
                finally:
                    if thread_cr:
                        # thread_cr.commit()
                        thread_cr.close()
            else:
                _logger.error('{} thread_main %s registry %s not ready - aborting', cls.get_os_id(), db_name, registry)
        _logger.debug('{} thread_main terminated'.format(cls.get_os_id()))

    @classmethod
    def run_listener(cls,env,subscribe_to=None, exit_cond=None):
        import select

        conn= env.cr._cnx
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        env.cr.execute('LISTEN aroolla_solo;')
        # explicit default subscription list
        subscribe_to = subscribe_to or ('exit')
        if not exit_cond:
            exit_cond = cls.exit_cond
        _logger.info('{} run_listener subscribe to:{} on channel aroolla_solo'.format(cls.get_os_id(), subscribe_to))
        _logger.info(
            '{} active threads:{} {} '.format(cls.get_os_id(), threading.active_count(), odoo.sql_db._Pool))
        while 1:
            if select.select([conn], [], [], 60) == ([], [], []):
                # pass
                _logger.debug('{} run_listener({}) Timeout'.format(cls.get_os_id(),subscribe_to))
            else:
                conn.poll()
                while conn.notifies:
                    notify = conn.notifies.pop(0)
                    if notify.payload in subscribe_to:
                        if notify.payload == 'exit':
                            if exit_cond:
                                with exit_cond:
                                    exit_cond.notify()
                                _logger.info('{} run_listener({}) exit notified'.format(
                                    cls.get_os_id(), subscribe_to))
                            else:
                                _logger.info('{} run_listener({}) exit - nothing todo'.format(
                                    cls.get_os_id(), subscribe_to))
                        else:
                            _logger.warning(
                                '{} run_listener({}) {} notified but not supported'.format(cls.get_os_id(), subscribe_to, notify.payload))
                    else:
                        _logger.debug(
                            '{} run_listener({}) {} ignored'.format(cls.get_os_id(), subscribe_to, notify.payload))


    @classmethod
    def notify_all(cls, env, payload):
        _logger.info('{} AroollaThread.notify_all({}) exit - nothing todo'.format(cls.get_os_id(), payload))
        # env.cr.execute("NOTIFY aroolla_solo, %s;", payload)
        env.cr.execute("NOTIFY aroolla_solo, 'exit';")

    @classmethod
    def main_job(cls, env, worker):
        _logger.warning('AroollaThread main_job not implemented -> NOOP')

def get_full_thread_name(thread=None):
    if thread is None:
        thread = threading.current_thread()
    name = 'p#{} t#{} '.format(os.getpid(),thread.ident)
    try:
        name += '[{}]'.format(thread.name)
    except AttributeError as e:
        pass
    return name
