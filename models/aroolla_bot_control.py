# -*- coding: utf-8 -*-
# Part of Aroolla. See LICENSE file for full copyright and licensing details.

import logging
import random
import threading
from datetime import datetime
import os

from odoo import api, models

from .aroolla_thread import AroollaThread

_logger = logging.getLogger(__name__)

BUSINESS_SCENARIOS_TO_COMPLETE = 6
MAX_PARALLEL_BUSINESS_SCENARIOS = 2
MIN_TIME_BETWEEN_BUSINESS_SCENARIOS = 0
MAX_TIME_BETWEEN_BUSINESS_SCENARIOS = 0
TOUR_END_STATE = 'tour_done'

# sets aroolla_thread max execution time
# to allow cron worker recycling (must be < odoo.conf real_time_limit)
AROOLLA_THREAD_LIMIT_TIME = 3600


class AroollaBotControl(models.AbstractModel, AroollaThread):
    _name = 'aroolla_base_bot.aroolla_bot_control'
    _description = 'Aroolla Bot Control'

    @api.model
    def main_action(self):
        solo_event = self.env.ref('aroolla_game_roasteria_solo_data.event_event_1')
        if not solo_event.active:
            _logger.debug('AroollaBotControl.main_action: solo_event inactive - no thread started')
            # do nothing cron action
            return
        _logger.debug('AroollaBotControl.main_action: start_thread => main_job')
        self.start_thread(on_server_startup=False, limit_time=AROOLLA_THREAD_LIMIT_TIME)

    @api.model
    def stop_workers(self):
        AroollaThread.notify_all(self.env, 'exit')

    @classmethod
    def main_job(cls, env, worker):
        """
        Implements AroollaThread main_job method
        called repeatedly from thread_main
        """
        # loop over distinct users needing business scenario processing
        # grouping usr_scn_state by user
        users = [d['user'][0] for d in
                 env['aroolla_base_bot.usr_scn_state'].read_group(
                     ['|', ('scenario.type', '!=', 'tour'), ('bot_state', '=', TOUR_END_STATE)],
                     ['scenario'], 'user')]
        # filtering out game terminated users
        terminated_users = env['aroolla_base_bot.usr_scn_state'].search(
            [('user.id', 'in', users), ('scenario.name', '=', 'ttfn'), ('bot_state', '=', 'done') ]).mapped('user').ids
        users = set(users) - set(terminated_users)
        # jobs = env['aroolla_base_bot.job'].create([dict(user=user) for user in users])
        # we don't create job yet to keep track of work but directly lock users instead
        if not users:
            _logger.debug('{} nothing to do'.format(worker.display_name))
            return
        # load user objects
        users = [env['res.users'].browse(user_id) for user_id in users]

        for user in worker.locked_jobs(users):
            env.cr.commit()
            user.invalidate_cache()
            values = {'aroolla_player_id': user.partner_id.id}
            _logger.debug('{} running {}:[{}] job'.format(worker.display_name, user.partner_id, user.partner_id.name))
            try:
                business_states = user.aroollabot_states.filtered(lambda r: r.scenario.type == 'business')
                done_business_states = business_states.filtered(lambda r: r.bot_state == 'done')
                active_business_states = business_states - done_business_states

                congrats_states = user.aroollabot_states.filtered(lambda r: r.scenario.type == 'congrats')
                active_congrats_states = congrats_states.filtered(lambda r: r.bot_state != 'done')
                _logger.debug(
                    '{} - active business_states = {} = total ({}) - done ({}) - active congrat_states ({}/{})'.format(
                        worker.display_name,
                        len(active_business_states), len(business_states), len(done_business_states),
                        len(active_congrats_states), len(congrats_states)))
                if len(active_business_states) < MAX_PARALLEL_BUSINESS_SCENARIOS:
                    last_scenario_state_update = user.aroollabot_states.read_group([('user', '=', user.id)], [
                        'last_scenario_state_update:max(write_date)'], 'user')[0]['last_scenario_state_update']
                    time_since_state_change = (datetime.now() - last_scenario_state_update).seconds
                    min_time_before_next = random.randint(MIN_TIME_BETWEEN_BUSINESS_SCENARIOS,
                                                          MAX_TIME_BETWEEN_BUSINESS_SCENARIOS)
                    if len(done_business_states) >= BUSINESS_SCENARIOS_TO_COMPLETE:
                        # last business scenario done start congrats immediately
                        if len(congrats_states) == 0:
                            _logger.debug('{} - create congrat scenario'.format(worker.display_name))
                            env['aroolla_base_bot.usr_scn_state']. \
                                create_more_for_users([user], 1, scenario_domain=[('type', '=', 'congrats')])
                        else:
                            _logger.debug('{} - all scenarios done'.format(worker.display_name))
                    elif time_since_state_change > min_time_before_next and len(congrats_states) == 0 and len(business_states)<BUSINESS_SCENARIOS_TO_COMPLETE:
                        _logger.debug('{} - create business scenario'.format(worker.display_name))
                        env['aroolla_base_bot.usr_scn_state'].create_more_for_users([user], 1, scenario_domain=[
                            ('type', '=', 'business')])
                    else:
                        _logger.debug(
                            '{} - no new scenario - time_since_state_change={}s min_time_before_next={}s '.format(
                                worker.display_name, time_since_state_change, min_time_before_next))
                else:
                    _logger.debug('{} - no new scenario - active_business_states: {} >= {}'.format(worker.display_name,
                                                                                                   len(active_business_states),
                                                                                                   MAX_PARALLEL_BUSINESS_SCENARIOS))
                for bot in env['mail.channel'].sudo(user).get_aroollabots(values):
                    if bot['channel']:
                        _logger.debug('{} - {} [{}] ({}) _apply_logic'.format(worker.display_name, bot['channel'],
                                                                              bot['channel'].name,
                                                                              bot['channel'].channel_partner_ids))
                        values['aroolla_bot_id'] = bot['aroolla_bot_id']
                        # bot_control apply_logic
                        # only for business scenario (channel_partner_ids == 2) or congrats scenario (odoobot partner only)
                        # if (len(bot['channel'].channel_partner_ids) >1 and len(done_business_states) < BUSINESS_SCENARIOS_TO_COMPLETE) or len(active_congrats_states):
                        if len(bot['channel'].channel_partner_ids) > 1 or len(active_congrats_states):
                            env['mail.bot']._apply_logic(bot['channel'], values, command="bot_control")
                        else:
                            _logger.debug('{} skipped for {}'.format(worker.display_name, bot['channel']))
                    else:
                        _logger.warning('No bot_channel found in AroollaBotControl cannot apply logic')
                _logger.debug('{} finished {}:[{}] job. duration: {}s '.format(worker.display_name, user.partner_id,
                                                                               user.partner_id.name, (
                                                                                           datetime.now() - user.start_date).seconds))
            except Exception as e:
                _logger.error(
                    '{}  {}:[{}] job. encountered an Exception after {}s: {}'.format(worker.display_name,
                                                                                     user.partner_id,
                                                                                     user.partner_id.name, (
                                                                                                 datetime.now() - user.start_date).seconds,
                                                                                     e), exc_info=True)
                env.cr.rollback()
            else:
                env.cr.commit()
