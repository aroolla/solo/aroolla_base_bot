odoo.define('aroolla_game_solo.TourManager',
    function (require) {
        "use strict";

        // Define solo tour steps where a RPC call to set_step is necessary
        var solo_tour_step_rpc_required = [0, 4, 11, 19, 27, 34, 51, 55];

        var rpc = require('web.rpc');
        var session = require('web.session');
        var TourManager = require('web_tour.TourManager');
        var local_storage = require('web.local_storage');
        var utils = require('web_tour.utils');


        var __to_next_step = TourManager.prototype._to_next_step;
        var __activate_tip = TourManager.prototype._activate_tip;
        var _init = TourManager.prototype.init;

        TourManager.include({
            /**
             * Disables non Aroolla tours
             *
             * @override
             */
            _register: function (do_update, tour, name) {
                // Consuming all tours except ours (see web_tour tour_disable.js)
                if (!['roasteria_solo_oexp', 'roasteria_solo_business'].includes(name)) {
                    this.consumed_tours.push(name);
                }
                return this._super.apply(this, arguments);
            },
        });

        TourManager.prototype.init = function(parent, consumed_tours) {
            _init.call(this, parent, consumed_tours);
            this.subscribe();
        };

        TourManager.prototype._activate_tip = function (tip, tour_name, $anchor) {
            //console.log(_.str.sprintf("Aroolla TourManager _activate_tip tour %s", tour_name));
            __activate_tip.call(this, tip,  tour_name, $anchor);
             if (['roasteria_solo_oexp'].includes(tour_name)) {
                var tour = this.tours[tour_name], self = this;
                if (solo_tour_step_rpc_required.includes(tour.current_step)){
                    this._rpc({
                        model: 'aroolla_base_bot.tour',
                        method: 'set_step',
                        args: [[tour_name], [tour.current_step],['A']],
                    })
                        .then(function (result) {
                            if (!result || !('aroolla_bot_tour_cmd' in result)) {
                                console.log(_.str.sprintf("Aroolla TourManager _activate_tip activate_step %s, %d", tour_name, tour.current_step));
                            } else {
                                let aroolla_bot_tour_cmd = result['aroolla_bot_tour_cmd'];
                                console.log(_.str.sprintf("Aroolla TourManager _activate_tip %s activate_step %d => Aroolla bot changed to:%d", aroolla_bot_tour_cmd['tour'], tour.current_step, aroolla_bot_tour_cmd['step']));
                                // we can't defer previous synchronous __to_next_step call here as caller
                                // relies on tip active_tooltips update
                                self._deactivate_tip(tip);
                                tour.current_step = aroolla_bot_tour_cmd['step'];
                                __to_next_step.call(self, tour_name, 0);
                                //asynchronously re-called __to_next_step so need to call update ourself
                                self.update(tour_name);
                            }
                        });
                }else{
                    console.log(_.str.sprintf("Aroolla TourManager rpc_test rpc_not_called _activate_tip set_step %s, %d", tour_name, tour.current_step));
                }
            }
        };

        TourManager.prototype._to_next_step = function (tour_name, inc) {
            var tour = this.tours[tour_name], self = this;
            var consumed_step = tour.current_step;
            __to_next_step.call(this, tour_name, inc);
            if (['roasteria_solo_oexp'].includes(tour_name)) {
                if (solo_tour_step_rpc_required.includes(consumed_step)){
                    this._rpc({
                        model: 'aroolla_base_bot.tour',
                        method: 'set_step',
                        args: [[tour_name], [consumed_step],['C']],
                    })
                        .then(function (result) {
                            if (!result || !('aroolla_bot_tour_cmd' in result)) {
                                console.log(_.str.sprintf("Aroolla TourManager _to_next_step set_step %s, %d", tour_name, tour.current_step));
                            } else {
                                let aroolla_bot_tour_cmd = result['aroolla_bot_tour_cmd'];
                                console.log(_.str.sprintf("Aroolla TourManager _to_next_step %s set_step %d => Aroolla bot changed to:%d", aroolla_bot_tour_cmd['tour'], tour.current_step, aroolla_bot_tour_cmd['step']));

                                if (this.active_tooltips !== undefined){
                                    // deactivate current tip (destroy its widget) if any
                                    self._deactivate_tip(this.active_tooltips[tour_name]);
                                }
                                // we can't defer previous synchronous __to_next_step call here as caller
                                // relies on tip active_tooltips update
                                tour.current_step = aroolla_bot_tour_cmd['step'];
                                __to_next_step.call(self, tour_name, 0);
                                //asynchronously re-called __to_next_step so need to call update ourself
                                self.update(tour_name);
                            }
                        });
                }
                else{
                    console.log(_.str.sprintf("Aroolla TourManager rpc_test rpc_not_called _to_next_step set_step %s, %d", tour_name, tour.current_step));
                }

            }
        };
        TourManager.prototype.onNotification = function(notifications) {
            // Old versions passes single notification item here. Convert it to the latest format.
            if (notifications.length && typeof notifications[0][0] === 'string') {
                notifications = [notifications]
            }
            var message,channel;
            for (var i = 0; i < notifications.length; i++) {
                channel = notifications[i][0];
                console.log('notif#',i, 'channel:',channel);
                console.log('msg:',notifications[i][1]);
                // we filter our message only
                if (channel[0] === session.db && channel[1] === 'aroolla_base_bot.mail.bot' && channel[2] === session.uid) {
                    //and are only interrested in the last one
                    message = JSON.parse(notifications[i][1]);
                    console.log('message [', message, '] is for:', session.uid, 'handling it');
                    if ('aroolla_bot_tour_cmd' in message) {
                        let aroolla_bot_tour_cmd = message.aroolla_bot_tour_cmd;
                        // e.g bot_command = {'msg': msg, 'tour': 'roasteria_solo_oexp', 'step':0, 'location': '/web#home'}
                        let tour_name = aroolla_bot_tour_cmd.tour;
                        if (tour_name) {
                            let tour = this.tours[tour_name];
                            this._deactivate_tip(this.active_tooltips[tour_name]);
                            tour.current_step = aroolla_bot_tour_cmd['step'];
                            __to_next_step.call(this, tour_name, 0);
                            local_storage.setItem(utils.get_step_key(tour_name), tour.current_step);
                            // or calling ours to update backend but then step could change again todo verify
                            //this._to_next_step(tour_name, 0);
                            this.update(tour_name);
                        }

                        if ('location' in aroolla_bot_tour_cmd) {
                            let new_href = session.debug ? $.param.querystring(aroolla_bot_tour_cmd.location, {debug: session.debug}) : aroolla_bot_tour_cmd.location;
                            //framework.redirect(new_href);
                            window.location = new_href;
                            //not very useful as page is about to change but testing
                            if (tour_name) {
                                this.update(tour_name);
                            }
                        }
                    }
                }
            }
            if (message) {
                console.log('aroolla_tour_manager onNotification[' + channel + '] => ' + JSON.stringify(message));

                if (session.user_companies)
                //only user with multiple companies have current_company name in session
                    this.team = session.user_companies.current_company[1];
                //if (session.is_admin)
                //
            }
        };

        TourManager.prototype.subscribe = function() {
            this.call('bus_service', 'onNotification', this, this.onNotification);
            console.log('Tour Manager subscribed to aroolla_base_bot.mail.bot channel');
        };
 });

