odoo.define('aroolla_game_solo.MailBotService', function (require) {
"use strict";

    var session = require('web.session');

    var MailBotService = require('mail_bot.MailBotService');

    /**
     * @override
     */

    // var _start = MailBotService.prototype.start;

    // shorten odoobot init 2min default => 10s for aroolla solo
/*
    MailBotService.prototype.start =
        function () {
        var self = this;
        if ('odoobot_initialized' in session && ! session.odoobot_initialized) {
            setTimeout(function () {
                session.odoobot_initialized = true;
                self._rpc({
                    model: 'mail.channel',
                    method: 'init_odoobot',
                });
            }, 500);
        }
    };
*/

    return MailBotService;
}
);

