# -*- coding: utf-8 -*-
# Part of Aroolla. See LICENSE file for full copyright and licensing details.

{
    'name': 'OdooBot for Aroolla',
    'version': '1.0',
    'category': 'Discuss',
    'summary': 'Add Aroolla support for OdooBot',
    'description': "",
    'website': 'https://odoosim.ch/cases/paper/admin/',
    'depends': ['mail_bot', 'im_livechat'],
    'installable': True,
    'application': False,
    'auto_install': True,
    'data': [
        'data/aroolla_bot_control_cron.xml',
        'data/ir.config_parameter.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/layout.xml'
    ]
}
