# -*- coding: utf-8 -*-

from odoo.addons.bus.controllers.main import BusController
from odoo.http import request

import logging

_logger = logging.getLogger(__name__)


class AroollaBotBusController(BusController):

    # --------------------------
    # Extends BUS Controller Poll to add our own channel
    # --------------------------
    def _poll(self, dbname, channels, last, options):
        if request.session.uid:
            channels = list(channels)  # do not alter original list
            channels.append((request.db, 'aroolla_base_bot.mail.bot', request.session.uid))
            _logger.info('AroollaBotBusController._poll aroolla_base_bot,tour_control added to channels:{}'.format(channels))
        else:
            _logger.warning('AroollaBotBusController._poll no uid in request.session:{}'.format(request.session))
        return super(AroollaBotBusController, self)._poll(dbname, channels, last, options)
